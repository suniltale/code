package yubtube.com.youbtube;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class WebActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        //String frameVideo = "<iframe width="560" height="315" src="https://www.youtube.com/embed/V7LW9srvJHU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>";
        //String frameVideo = "<html><body>Video From YouTube<br><iframe width=\"420\" height=\"315\" src=\"https://www.youtube.com/embed/47yJ2XCRLZs\" frameborder=\"0\" allowfullscreen></iframe></body></html>";

           String frameVideo ="<iframe src=\"https://player.vimeo.com/video/194010048\" width=\"200\" height=\"200\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\n" +
                   "<p><a href=\"https://vimeo.com/194010048\">SHAWLS AND SCARVES</a> from <a href=\"https://vimeo.com/user14562621\">Messe Frankfurt France</a> on <a href=\"https://vimeo.com\">Vimeo</a>.</p>";


        WebView displayVideo = (WebView)findViewById(R.id.webView);
        displayVideo.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = displayVideo.getSettings();
        webSettings.setJavaScriptEnabled(true);
        displayVideo.loadData(frameVideo, "text/html", "utf-8");
    }



}
