package yubtube.com.youbtube;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

/**
 * Created by Admin on 6/18/2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.VideoInfoHolder> {
    //these ids are the unique id for each video
   String[] VideoID = {"P3mAtvs5Elc", "nCgQDjiotG0", "P3mAtvs5Elc"};
//    String frameVideo ="<iframe src=\"https://player.vimeo.com/video/194010048\" width=\"640\" height=\"360\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\n" +
//            "<p><a href=\"https://vimeo.com/194010048\">SHAWLS AND SCARVES</a> from <a href=\"https://vimeo.com/user14562621\">Messe Frankfurt France</a> on <a href=\"https://vimeo.com\">Vimeo</a>.</p>";
//
//
//    String frameVideo1 ="<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/8uBnxSScf_A\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>";
//
//
//    String frameVideo2 ="<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/vKAPPTw9uS8\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>";
//
//
//  String[] VideoID = {frameVideo,frameVideo1,frameVideo2};
//
//
    Context ctx;

    public RecyclerAdapter(Context context) {
        this.ctx = context;
    }

    @Override
    public VideoInfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_row, parent, false);
        return new VideoInfoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final VideoInfoHolder holder, final int position) {


        final YouTubeThumbnailLoader.OnThumbnailLoadedListener  onThumbnailLoadedListener = new YouTubeThumbnailLoader.OnThumbnailLoadedListener(){
            @Override
            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

            }

            @Override
            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                youTubeThumbnailView.setVisibility(View.VISIBLE);
                holder.relativeLayoutOverYouTubeThumbnailView.setVisibility(View.VISIBLE);
            }
        };

//        holder.youTubeThumbnailView.initialize(Resource.KEY, new YouTubeThumbnailView.OnInitializedListener() {
//            @Override
//            public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
//
//                youTubeThumbnailLoader.setVideo(VideoID[position]);
//
//                youTubeThumbnailLoader.setOnThumbnailLoadedListener(onThumbnailLoadedListener);
//            }
//
//            @Override
//            public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
//                //write something for failure
//            }
//        });



        holder.playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) ctx, Resource.KEY, VideoID[position]);
//                ctx.startActivity(intent);

                ctx.startActivity(new Intent(ctx,Second.class));
            }
        });


    }


    @Override
    public int getItemCount() {
        return VideoID.length;
    }
    public class VideoInfoHolder extends RecyclerView.ViewHolder implements View.OnClickListener

    {
        protected RelativeLayout relativeLayoutOverYouTubeThumbnailView;
        YouTubeThumbnailView youTubeThumbnailView;
        protected ImageView playButton;
        WebView displayVideo;

        public VideoInfoHolder(View itemView) {
            super(itemView);
         //   displayVideo = (WebView) itemView.findViewById(R.id.webView);
              playButton=(ImageView)itemView.findViewById(R.id.btnYoutube_player);
//             playButton.setOnClickListener(this);
//               relativeLayoutOverYouTubeThumbnailView = (RelativeLayout) itemView.findViewById(R.id.relativeLayout_over_youtube_thumbnail);
//               youTubeThumbnailView = (YouTubeThumbnailView) itemView.findViewById(R.id.youtube_thumbnail);


        }

        @Override
        public void onClick(View v) {
            Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) ctx, Resource.KEY, "P3mAtvs5Elc");
            ctx.startActivity(intent);
        }
    }
}
